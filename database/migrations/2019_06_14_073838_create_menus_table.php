<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('lang_id')->nullable();
            $table->string('name');
            $table->string('img');
            $table->text('desc')->nullable();
            $table->integer('price');
            $table->tinyInteger('offer')->nullable();
            $table->text('seo')->nullable();
            $table->string('slug')->nullable();
            //  view or not ( home page  or site )
            $table->enum('show_in_homePage',['yes','no'])->default('yes');
            $table->enum('show',['yes','no'])->default('yes');
            $table->enum('special',['yes','no'])->default('yes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
