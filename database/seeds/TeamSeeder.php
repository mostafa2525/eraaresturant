<?php

use Illuminate\Database\Seeder;
use App\Models\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Team();
        $data->name = "Jessica Doe";
        $data->poss = " Chef";
        $data->img = "1.jpg";
        $data->save();



        $data = new Team();
        $data->name = "Angelina Doe";
        $data->poss = " Chef";
        $data->img = "2.jpg";
        $data->save();


        $data = new Team();
        $data->name = "John Doe";
        $data->poss = " Chef";
        $data->img = "3.jpg";
        $data->save();
    }
}
