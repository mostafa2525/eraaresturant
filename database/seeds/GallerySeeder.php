<?php

use Illuminate\Database\Seeder;
use App\Models\Gallery;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Gallery();
        $data->img = "1.jpg";
        $data->save();


        $data = new Gallery();
        $data->img = "2.jpg";
        $data->save();



        $data = new Gallery();
        $data->img = "3.jpg";
        $data->save();


        $data = new Gallery();
        $data->img = "4.jpg";
        $data->save();


        $data = new Gallery();
        $data->img = "5.jpg";
        $data->save();


        
    }
}
