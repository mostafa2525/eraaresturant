<?php

use Illuminate\Database\Seeder;
use App\Models\Category;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Category();
        $data->name = "Lunch";
        $data->save();



        $data = new Category();
        $data->name = "Dinner";
        $data->save();



        $data = new Category();
        $data->name = "Drinks";
        $data->save();
    }
}
