<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;


class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = array();
        $name[0] = '';
    	$name[1] = "CONSECTETUR ADIPISCING";
    	$name[2] = "ADIPISCING";
    	$name[3] = "PULVINAR MAGNA";
    	$name[4] = "FEUGIAT NIBH";
    	$name[5] = "FEUGIAT";
    	$name[6] = "NIBH";
    	$name[7] = "VESTIBULUM";
    	$name[8] = "CURABITUR PELLENTESQUE";
    	$name[9] = "PELLENTESQUE";
    	$name[10] = "SIT FOLOR";
    	$name[11] = "COLOR";
    	$name[12] = "LIGHT AMET";

    	for($i=1;$i<=12;$i++)
    	{
    		$data = new Menu();
	        $data->cat_id = "1";
	        $data->name = $name[$i];
	        $data->desc = "Lorem, dolor sit, amet.";
	        $data->img = $i.".jpg";
	        $data->price = $i+5;        
	        $data->save();
    	}


    	for($i=12;$i>=1;$i--)
    	{
    		$data = new Menu();
	        $data->cat_id = "2";
	        $data->name = $name[$i];
	        $data->desc = "Lorem, dolor sit, amet.";
	        $data->img = $i.".jpg";
	        $data->price = $i+5;        
	        $data->save();
    	}



    	for($i=1;$i<=12;$i++)
    	{
    		$data = new Menu();
	        $data->cat_id = "3";
	        $data->name = $name[$i];
	        $data->desc = "Lorem, dolor sit, amet.";
	        $data->img = $i.".jpg";
	        $data->price = $i+5;        
	        $data->save();
    	}



       
    }
}
