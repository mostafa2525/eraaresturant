<?php

use Illuminate\Database\Seeder;
use App\Models\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $data = new Slider();
	   $data->name = "WELCOME TO";
	   $data->title1 = "THE PORTO";
	   $data->title2 = "The best place to eat in downtown Porto!";
	   $data->img = "1.jpg";
	   $data->link = "#";
	   $data->save();




	   $data = new Slider();
	   $data->name = "Best ingredients, freshly prepared!";
	   $data->title1 = "DELICIOUS!!!";
	   $data->title2 = "The best place to eat in downtown Porto!";
	   $data->img = "2.jpg";
	   $data->link = "#";
	   $data->save();



    }
}
