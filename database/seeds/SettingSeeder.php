<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new Setting();
        $data->site_name = "eraa restaurant";
        $data->email = "info@reataurant.com";
        $data->facebook = "https://www.facebook.com/";
        $data->twitter = "https://twitter.com/";
        $data->instagram = "https://www.instagram.com/";
        $data->youtube = "https://www.youtube.com/";
        $data->fax = "123456";
        $data->mobile = "(800) 123-4567";
        $data->phone = "(800) 123-4567";
        $data->address = "1234 Street Name, City Name";
        $data->logo = "logo.png";
        $data->whatsapp = "(800) 123-4567";
        $data->save();
    }
}
