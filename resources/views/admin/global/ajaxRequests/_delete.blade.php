


<script type="text/javascript">
  
$(document).on("click",".delete-row",function(){

  var el = $(this);
  var route = $(this).attr("data-route")
  $.ajax({
      type: "GET",
      url: route,
      cache: false,
      beforeSend:function()
      {
          $("#coverloading").css("display","block");
          el.prop( "disabled", true );
      },
      success: function (data) 
      {
          $("#coverloading").css("display","none");
          el.parents("tr").remove();


      }, error: function (data) 
      {
          $("#coverloading").css("display","none");
          el.prop( "disabled", false );
        
      }
  });



});

</script>