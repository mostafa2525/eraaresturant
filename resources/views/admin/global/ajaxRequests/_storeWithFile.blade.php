<script src="{{aurl()}}/seoera/vue/vue.js" type="text/javascript"></script>
<script src="{{aurl()}}/seoera/vue/axios.min.js"></script>


<script type="text/javascript">
  
  
  
  new Vue({


  el: '#app',
  data:{
      info: "Save"
  },

  methods:{

      sendData: function()
      {

        let formData  = new FormData(jQuery('#form-edit')[0]);
        let imagefile = document.querySelector('#file')
        formData.append('image', imagefile.files[0])


        axios.post("{{ $route }}",formData,{
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then( function (response) 
      {
        if(response.status==200)
        {
          $("#errors").html("")
          $("#errors").append("<li class='alert alert-success show-errors'>@lang('site.updated_success')</li>")
          alert("@lang('site.updated_success')")
           
        }
      })
      .catch(function(error)
      {
        $.each(error, function (key, item) 
        {
          $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
        });
      })


    

    }
  }

})



</script>