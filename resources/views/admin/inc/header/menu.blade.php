<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-anchor"></i>@lang('admin.admins') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>



                <li class="has-submenu">
                    <a href="#">
                        <i class=" dripicons-contract"></i>@lang('admin.booking') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                    </ul>
                </li>

                

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-menu"></i>@lang('admin.categories') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>


                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-store"></i>@lang('admin.menu') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>



                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-toggles"></i>@lang('admin.slider') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-photo-group"></i>@lang('admin.gallery') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>


                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-blog"></i>@lang('admin.blog') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-user-group"></i>@lang('admin.team') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>

                


                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-mail"></i>@lang('admin.contacts') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.messages')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.subscribers')</a>
                        </li>
                    </ul>
                </li>

                <li class="has-submenu">
                    <a href="#">
                        <i class="dripicons-user"></i>@lang('admin.testimonials') <div class="arrow-down"></div></a>
                    <ul class="submenu">
                        <li>
                            <a href="">@lang('admin.view')</a>
                        </li>
                        <li>
                            <a href="">@lang('admin.add')</a>
                        </li>
                    </ul>
                </li>





        


            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
<!-- end navbar-custom -->