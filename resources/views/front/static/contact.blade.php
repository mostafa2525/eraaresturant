
@extends('front.main')

@section('content')



<div role="main" class="main">
	<section class="page-header page-header-modern page-header-md bg-transparent mb-0 pb-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 align-self-center p-static order-2 text-center">
					<h1 class="text-10 text-color-dark my-3">Get in <strong>Touch</strong></h1>
					<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
					<hr class="custom-divider my-0">
				</div>
			</div>
		</div>
	</section>

	<section class="section section-default mb-0">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-md-6 col-lg-12">
							<h5 class="mb-1 mt-4">Book Now</h5>
							<p><i class="fas fa-phone"></i> {{$setting->mobile}} </p>

							<h5 class="mb-1 mt-4">Private Events</h5>
							<p><i class="fas fa-phone"></i> {{$setting->mobile}} </p>

							<h5 class="mb-1 mt-4">Visit Us</h5>
							<p><i class="fas fa-map-marker-alt"></i> {{$setting->address}}</p>
						</div>
						<div class="col-md-6 col-lg-12">
							<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
							<div id="googlemaps" class="google-map small mt-md-4 mt-lg-0">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13815.43989369202!2d31.225720279345705!3d30.040874841784515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x145840d12f1cc3c1%3A0xde3e1b56b22e91f8!2sCairo+Opera+House!5e0!3m2!1sen!2seg!4v1560559206612!5m2!1sen!2seg"  height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>

							<p><a href="http://maps.google.com/" target="_blank">(Get Directions)</a></p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<h5 class="mb-3 mt-4">Send a Message</h5>

					<div class="form-row">
	                        <div class="col-md-12">
	                            <ul id="errors"></ul>
	                        </div>
						</div>

					<form id="contactForm" class="contact-form22" action="" method="POST">
						@csrf
						
						<div class="form-row">
							<div class="form-group col">
								<label>Your name *</label>
								<input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label>Your email address *</label>
								<input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label>Subject</label>
								<input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<label>Message *</label>
								<textarea maxlength="5000" data-msg-required="Please enter your message." rows="3" class="form-control" name="message" id="message" required></textarea>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col">
								<input type="submit" value="Send Message" class="btn btn-lg btn-primary" data-loading-text="Loading..." id="submitMessage">

								<div class="contact-form-success alert alert-success d-none" id="contactSuccess">
									Message has been sent to us.
								</div>

							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>




@endsection



@section('script')
<script type="text/javascript">


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contactForm").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#contactForm')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.static.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           		$("#submitMessage").attr("disabled",true);
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.form-control').val("");
           	 $("#submitMessage").attr("disabled",false);
           	 $(".contact-form-success").css('display','block');

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
           		$("#submitMessage").attr("disabled",false);
              
            }

        });

	});

</script>



@endsection
