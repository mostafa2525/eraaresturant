
@extends('front.main')

@section('content')



<div role="main" class="main">
				<section class="page-header page-header-modern page-header-background page-header-background-sm parallax" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="{{furl()}}/img/demos/restaurant/parallax-restaurant-2.jpg">
					<div class="container">
						<div class="row my-4">
							<div class="col-md-12 align-self-center p-static order-2 text-center">
								<h1 class="text-10 py-3 mb-3 text-color-light">The <strong>Restaurant</strong></h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row">
						<div class="col-lg-12 pt-4">
							<h2>The best place to eat in downtown Porto!</h2>

							<p class="lead mb-5 mt-4">Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat.</p>

							<img width="205" class="img-fluid float-left mr-4 mb-4 mt-1" alt="" src="{{furl()}}/img/demos/restaurant/gallery/restaurant-gallery-3.jpg">

							<p>Lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat.</p>

							<p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>

							<p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor.</p>

						</div>
					</div>
				</div>

				<section class="section section-default mb-0">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">

								<h4 class="mt-4 mb-2">Get in <strong>Touch</strong></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

								<hr class="custom-divider">

								<h5 class="mb-1 mt-4">Book Now</h5>
								<p><i class="fas fa-phone"></i> {{$setting->mobile}} </p>

								<h5 class="mb-1 mt-4">Visit Us</h5>
								<p><i class="fas fa-map-marker-alt"></i> {{$setting->address}} </p>

							</div>
						</div>
					</div>
				</div>
			</div>




@endsection
