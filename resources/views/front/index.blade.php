
@extends('front.main')

@section('content')

	<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 650px;">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 650, 'disableProgressBar': 'on'}">
						<ul>

							@foreach($slides as $sl)
							<li data-transition="fade">
								<img src="{{getImage(SLI_PATH.$sl->img)}}"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption top-label alternative-font"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-55"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">{{$sl->name}}</div>

								

								<div class="tp-caption main-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-5"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">{{$sl->title1}}</div>

								<div class="tp-caption bottom-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="40"
									data-start="2000"
									style="z-index: 5; font-size: 1.2em;"
									data-transform_in="y:[100%];opacity:0;s:500;">{{$sl->title2}}</div>

								<a class="tp-caption btn btn-md btn-primary"
									data-hash
									data-hash-offset="85"
									href="#menu"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="85"
									data-start="2200"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">Our Menu</a>

							</li>
							@endforeach
						
						</ul>
					</div>
				</div>
				<section class="pt-5">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-2">Enjoy <strong>Your Meal</strong></h2>
								<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

								<hr class="custom-divider">
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="0">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="{{furl()}}/img/demos/restaurant/blog/blog-restaurant-1.png" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="{{furl()}}/img/demos/restaurant/icons/restaurant-icon-1.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text px-4">
												<h2 class="mb-3 mt-1">Sweets</h2>
												<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
												<a class="btn btn-primary mt-3" href="{{route('front.get.menu.index')}}">View More <i class="fas fa-long-arrow-alt-right"></i></a>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="{{furl()}}/img/demos/restaurant/blog/blog-restaurant-2.png" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="{{furl()}}/img/demos/restaurant/icons/restaurant-icon-2.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text px-4">
												<h2 class="mb-3 mt-1">Coffee &amp; Beer</h2>
												<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
												<a class="btn btn-primary mt-3" href="{{route('front.get.menu.index')}}">View More <i class="fas fa-long-arrow-alt-right"></i></a>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="{{furl()}}/img/demos/restaurant/blog/blog-restaurant-3.png" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="{{furl()}}/img/demos/restaurant/icons/restaurant-icon-3.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text px-4">
												<h2 class="mb-3 mt-1">Cake &amp; Cookies</h2>
												<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
												<a class="btn btn-primary mt-3" href="{{route('front.get.menu.index')}}">View More <i class="fas fa-long-arrow-alt-right"></i></a>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>

				<section class="section section-background section-center" style="background-image: url({{furl()}}/img/demos/restaurant/parallax-restaurant.jpg); background-position: 50% 100%; min-height: 615px;">
					<div class="container">
						<div class="row justify-content-center">
							<div class="col-lg-8">
								<hr class="custom-divider">
								<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">

									@foreach($testi as $tes)
									<div>
										<div class="col">
											<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
												<blockquote>
													<p>{{$tes->desc}}</p>
												</blockquote>
												<div class="testimonial-author">
													<p><strong>{{$tes->name}}</strong><span>{{$tes->poss}}</span></p>
												</div>
											</div>
										</div>
									</div>
									@endforeach
							
								</div>
							</div>
						</div>
						<div class="row justify-content-center">
							<div class="col-lg-8">

							</div>
						</div>
					</div>
				</section>
				<section class="pt-3 pb-3">
					<div class="container">
						<div class="row mb-5">
							<div class="col-lg-12 text-center">
								<h4 class="mt-4 mb-2">Our <strong>Gallery</strong></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

								<hr class="custom-divider">

								<div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
									<div class="masonry-loader masonry-loader-showing">
										<div class="masonry" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">

											@foreach($gallery as $gall)
											<div class="masonry-item @if($loop->iteration == 2) w2 @endif">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="{{getImage(GALL_PATH.$gall->img)}}" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="{{getImage(GALL_PATH.$gall->img)}}">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
											@endforeach
									
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="container-fluid">
					<div class="row mt-5">

						<div class="col-lg-6 p-0">
							<section class="section section-quaternary section-no-border h-100 mt-0">
								<div class="row justify-content-end">
									<div class="col-half-section col-half-section-right">
										<div class="text-center">
											<h4 class="mt-3 mb-0 heading-dark">Our <strong>Blog</strong></h4>
											<p class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

											<hr class="custom-divider m-0">
										</div>
										
										<div class="owl-carousel owl-theme show-nav-title mt-5 mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'autoplay': true, 'autoplayTimeout': 5000}">
											
											
											
												@foreach($posts as $pos)
												@if($loop->iteration==1  or $loop->iteration==3 or $loop->iteration==5 )
												<div>
												@endif

												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-3">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="{{getImage(BLOG_PATH.$pos->img)}}" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">{{$pos->name}}</h4>
															<p class="text-3 pt-3 pb-1">{{\Str::limit($pos->small_desc,80,' ..')}}</p>
															<a class="mt-2" href="#">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
														</div>
													</div>
												</div>
												
												@if($loop->iteration==2  or $loop->iteration==4 or $loop->iteration==6 )
												</div>
												@endif


												@endforeach 

												


								




										</div>

									</div>
								</div>
							</section>
						</div>

						<div class="col-lg-6 p-0">
							<section class="section section-tertiary section-no-border h-100 mt-0">
								<div class="row">
									<div class="col-half-section">
										<div class="text-center">
											<h4 class="mt-3 mb-0 heading-dark">Our <strong>Team</strong></h4>
											<p class="mb-1">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

											<hr class="custom-divider m-0">
										</div>

										<div class="owl-carousel owl-theme show-nav-title mt-5 mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 2}, '1199': {'items': 2}}, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">

											@foreach($team as $te)
											<div>
												<div class="thumb-info thumb-info-no-zoom thumb-info-no-borders mb-0">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="{{getImage(TEAM_PATH.$te->img)}}" class="img-fluid" alt="">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text thumb-info-caption-text-custom text-center">
															<h4 class="mb-0 mt-1 heading-dark">{{$te->name}}</h4>
															<p class="text-3 p-0 m-0 mb-2">{{$te->poss}}</p>
														</div>
													</div>
												</div>
											</div>
											@endforeach
											
										</div>
									</div>
								</div>
							</section>
						</div>

					</div>
				</div>

				<section id="menu" style="background-image: url({{furl()}}/img/demos/restaurant/background-restaurant.png); background-position: 50% 100%; background-repeat: no-repeat;">
					<div class="container">
						<div class="row mt-3">
							<div class="col-lg-12 text-center">
								<h4 class="mt-4 mb-2">Special <strong>Menu</strong></h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

								<hr class="custom-divider">

								<ul class="special-menu pb-4">
									@foreach($menu as $men)
									<li>
										<img src="{{getImage(MENU_PATH.$men->img)}}" class="img-fluid" alt="">
										<h3>{{$men->name}} <em>Special</em></h3>
										<p><span>{{$men->desc}}</span></p>
										<strong class="special-menu-price text-color-dark">$ {{$men->price}}</strong>
									</li>
									@endforeach
								
								</ul>

							</div>
						</div>
						<div class="row mb-0 mt-5">
							<div class="col-lg-12 text-center">
								<a href="{{route('front.get.menu.index')}}" class="btn btn-primary btn-lg mb-5">Full Menu</a>
							</div>
						</div>
					</div>
				</section>
	</div>

@endsection
