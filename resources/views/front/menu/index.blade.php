
@extends('front.main')

@section('content')

	<div role="main" class="main">
				<section class="page-header page-header-modern page-header-md bg-transparent mb-0 pb-3">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-self-center p-static order-2 text-center">
								<h1 class="text-10 text-color-dark my-3">Food <strong>&amp; </strong> Drink</h1>
								<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
								<hr class="custom-divider my-0">
							</div>
						</div>
					</div>
				</section>

				<section class="section section-default">
					<div class="container">
						<div class="row">
							<div class="col">
								
								<div class="tabs tabs-bottom tabs-center tabs-simple">
									<ul class="nav nav-tabs">

										@foreach($categories as $cat)
										<li class="nav-item @if($loop->iteration == '1') active @endif">
											<a class="nav-link" href="#{{$cat->name.$cat->id}}" data-toggle="tab">{{$cat->name}}</a>
										</li>
										@endforeach
									</ul>

									<div class="tab-content">


									@foreach($categories as $cat)

										<div class="tab-pane @if($loop->iteration == '1') active @endif" id="{{$cat->name.$cat->id}}">
											<div class="row">
												@foreach($cat->menu as $men)
												<div class="col-lg-4">
													<div class="menu-item">
														<span class="menu-item-price">$ {{ $men->price}}</span>
														<h4>{{ $men->name}}</h4>
														<p>{{ $men->desc}}</p>
													</div>
												</div>
												@endforeach
											</div>
										</div>

									@endforeach

									</div>
								</div>

							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row mt-5 mb-4">
						<div class="col-lg-12 text-center">
							<h4 class="mt-4 mb-2">Special <strong>Menu</strong></h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>

							<hr class="custom-divider">

							<ul class="special-menu pb-5">
								@foreach($menu as $men)
								<li>
									<img src="{{getImage(MENU_PATH.$men->img)}}" class="img-fluid" alt="">
									<h3>{{$men->name}} <em>Special</em></h3>
									<p><span>{{$men->desc}}</span></p>
									<strong class="special-menu-price text-color-dark">$ {{$men->price}}</strong>
								</li>
								@endforeach
							
							</ul>

						</div>
					</div>
				</div>
			</div>

@endsection
