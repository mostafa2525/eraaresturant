
			<footer id="footer" class="border-top-0 bg-color-secondary mt-0">
				<div class="container">
					<div class="row py-5">
						<div class="col text-center">
							<ul class="footer-social-icons social-icons social-icons-clean social-icons-big social-icons-opacity-light social-icons-icon-light mt-1">

								<li class="social-icons-facebook">
									<a href="{{$setting->facebook}}" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>

								<li class="social-icons-twitter"><a href="{{$setting->twitter}}" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="{{$setting->instagram}}" target="_blank" title="Linkedin"><i class="fab fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright footer-copyright-style-2 bg-color-secondary">
					<div class="container py-2">
						<div class="row py-4">
							<div class="col-lg-8 text-center text-lg-left mb-2 mb-lg-0">
								<p>
									<span class="pr-0 pr-md-3 d-block d-md-inline-block"><i class="far fa-dot-circle text-color-primary top-1 p-relative"></i><span class="text-color-light opacity-7 pl-1">{{$setting->address}}</span></span>
									<span class="pr-0 pr-md-3 d-block d-md-inline-block"><i class="fab fa-whatsapp text-color-primary top-1 p-relative"></i><a href="tel:1234567890" class="text-color-light opacity-7 pl-1">{{$setting->whatsapp}}</a></span>
									<span class="pr-0 pr-md-3 d-block d-md-inline-block"><i class="far fa-envelope text-color-primary top-1 p-relative"></i><a href="mailto:mail@example.com" class="text-color-light opacity-7 pl-1">{{$setting->email}}</a></span>
								</p>
							</div>
							<div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end mb-4 mb-lg-0 pt-4 pt-lg-0">
								<p class="text-color-light opacity-7">© Copyright <a href="https://eraasoft.com/">EraaSoft</a> 2019. All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
		</div>

		<!-- Vendor -->
		<script src="{{furl()}}/vendor/jquery/jquery.min.js"></script>		
		<script src="{{furl()}}/vendor/jquery.appear/jquery.appear.min.js"></script>		
		<script src="{{furl()}}/vendor/jquery.easing/jquery.easing.min.js"></script>		
		<script src="{{furl()}}/vendor/jquery.cookie/jquery.cookie.min.js"></script>		

		<script src="{{furl()}}/vendor/popper/umd/popper.min.js"></script>		
		<script src="{{furl()}}/vendor/bootstrap/js/bootstrap.min.js"></script>		
		<script src="{{furl()}}/vendor/common/common.min.js"></script>		
		<script src="{{furl()}}/vendor/jquery.validation/jquery.validate.min.js"></script>	
		<script src="{{furl()}}/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>	
		<script src="{{furl()}}/vendor/jquery.gmap/jquery.gmap.min.js"></script>		
		<script src="{{furl()}}/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>	
		<script src="{{furl()}}/vendor/isotope/jquery.isotope.min.js"></script>		
		<script src="{{furl()}}/vendor/owl.carousel/owl.carousel.min.js"></script>		
		<script src="{{furl()}}/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>	
		<script src="{{furl()}}/vendor/vide/jquery.vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{furl()}}/js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="{{furl()}}/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>		<script src="{{furl()}}/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="{{furl()}}/js/views/view.contact.js"></script>

		
		<!-- Theme Custom -->
		<script src="{{furl()}}/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{furl()}}/js/theme.init.js"></script>


		@yield('script')



	</body>

<!-- Mirrored from preview.oklerthemes.com/porto/7.0.0/demo-restaurant.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 15:26:19 GMT -->
</html>
