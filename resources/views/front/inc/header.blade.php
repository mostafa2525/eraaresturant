<!DOCTYPE html>
<html data-style-switcher-options="{'changeLogo': false, 'borderRadius': 0, 'colorPrimary': '#e09b23', 'colorSecondary': '#344257', 'colorTertiary': '#D1E7E7', 'colorQuaternary': '#EDEADA'}">
	
<!-- Mirrored from preview.oklerthemes.com/porto/7.0.0/demo-restaurant.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2019 15:24:55 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Eraa - Restaurant</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="{{furl()}}/img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{furl()}}/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/magnific-popup/magnific-popup.min.css">



		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{furl()}}/css/theme.css">
		<link rel="stylesheet" href="{{furl()}}/css/theme-elements.css">
		<link rel="stylesheet" href="{{furl()}}/css/theme-blog.css">
		<link rel="stylesheet" href="{{furl()}}/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{furl()}}/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="{{furl()}}/vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{furl()}}/css/demos/demo-restaurant.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{furl()}}/css/skins/skin-restaurant.css">		<script src="master/style-switcher/style.switcher.localstorage.js"></script> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{furl()}}/css/custom.css">
		<link rel="stylesheet" href="{{furl()}}/css/front.css">

		<!-- Head Libs -->
		<script src="{{furl()}}/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target="#navSecondary" data-offset="170">

		<div class="body">