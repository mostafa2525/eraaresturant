
@extends('front.main')

@section('content')

<div role="main" class="main">
				<section class="page-header page-header-modern page-header-md bg-transparent mb-0 pb-3">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-self-center p-static order-2 text-center">
								<h1 class="text-10 text-color-dark my-3">Press <strong>&amp; </strong> Events</h1>
								<p class="text-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu pulvinar magna.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</p>
								<hr class="custom-divider my-0">
							</div>
						</div>
					</div>
				</section>

				<section class="section section-default mb-0">
					<div class="container pb-5">

						@foreach($posts as $post)
						<div class="row">
							<div class="col">
								<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-5">
									<div class="thumb-info-side-image-wrapper p-0 d-none d-sm-block">
										<a title="" href="{{route('front.get.press.show',[$post->slug])}}">
											<img src="{{getImage(BLOG_PATH.$post->img)}}" class="img-fluid mr-3" alt="" style="width: 235px;">
										</a>
									</div>
									<div class="thumb-info-caption">
										<div class="thumb-info-caption-text">
											<h2 class="mb-3 mt-1"><a title="" class="text-dark" href="{{route('front.get.press.show',[$post->slug])}}">{{$post->name}}</a></h2>
											<span class="post-meta">
												<span>{{$post->created_at}} | <a href="{{route('front.get.press.show',[$post->slug])}}">John Doe</a></span>
											</span>
											<p class="text-3 p-0 pl-md-3">{{$post->small_desc}}</p>
											<a class="mt-3" href="{{route('front.get.press.show',[$post->slug])}}">Read More <i class="fas fa-long-arrow-alt-right"></i></a>
										</div>
									</div>
								</div>

							</div>
						</div>
						@endforeach
						
					</div>
				</section>
			</div>

@endsection
