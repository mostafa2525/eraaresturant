<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Front')->name('front.')->group(function()
{

	Route::get('/','HomeController@index')->name('get.home.index');
	Route::get('/menu','MenuController@index')->name('get.menu.index');
	Route::get('/about','StaticController@about')->name('get.static.about');
	Route::get('/contact-us','StaticController@contactUs')->name('get.static.contactUs');
	Route::post('/contact-us','StaticController@sendMessage')->name('post.static.sendMessage');
	Route::get('/press','PressController@index')->name('get.press.index');
	Route::get('/press/{slig}','PressController@show')->name('get.press.show');
});
