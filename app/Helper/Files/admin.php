<?php   


if (!function_exists('adminAuth')) 
{
    function adminAuth()
    {
        return auth()->guard('admin')->user();
    }
}




// define link for assets of admin ( styles and scripts )
if (!function_exists('aurl')) 
{
    function aurl()
    {
         return url('admin');
    }
}




// display image from uploads folder
if (!function_exists('getImage')) 
{
    function getImage($path) 
    {
        return url('uploads/'.$path.'/');
    }
}







define("UPL_PATH", 'uploads/');
define("CAT_PATH", 'category/');
define("SETT_PATH", 'settings/');
define("SLI_PATH", 'slider/');
define("BLOG_PATH", 'blog/');
define("TEAM_PATH", 'team/');
define("TESTI_PATH", 'testimonials/');
define("MENU_PATH", 'menu/');
define("GALL_PATH", 'gallery/');