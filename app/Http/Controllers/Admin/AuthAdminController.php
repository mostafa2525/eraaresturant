<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthAdminController extends Controller
{
    
    // show login blade 
    public function login()
    {
        if(adminAuth())
        {
            return redirect(route('admin.get.home.index'));
        }
    	return view('admin.login');

    }// end show login function 



     // login authencation 
  	public function do_login(Request $request)
    {

    	$request->validate([
    		'email'=>'required|email|max:30',
    		'password'=>'required|max:30',
    	]);
    	$remember = request()->has('remember') ? true: false;
    	if(\Auth::guard('admin')->attempt(['email'=>request('email'),'password'=>request('password')],$remember))
    	{
    		// return redirect(url(language_symbole().'/adminPanel'));
    		return redirect(route('admin.get.home.index'));
    	}
    	else
    	{
    		return back()->withErrors(trans('admin.errors.dataNotCorrect'));
    	}
    }// end do login function 





    // admin logout 

    public function logout()
    {
    	auth()->guard('admin')->logout();
    	return redirect(route('admin.get.auth.login'));
    } // end logout function 




}
