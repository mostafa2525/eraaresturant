<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\Gallery;
use App\Models\Blog;
use App\Models\Team;
use App\Models\Menu;

class HomeController extends Controller
{
    public function index()
    {
    	$data['slides'] = Slider::where('show','yes')->take('3')->get();
    	$data['testi'] = Testimonial::where('show','yes')->take('3')->get();
    	$data['gallery'] = Gallery::where('show','yes')->take('5')->get();
    	$data['posts'] = Blog::where('show','yes')->take('6')->get();
    	$data['team'] = Team::where('show','yes')->take('6')->get();
    	$data['menu'] = Menu::where('show','yes')->where('show_in_homePage','yes')->take('6')->get();
    	return view('front.index')->with($data);
    }
}
