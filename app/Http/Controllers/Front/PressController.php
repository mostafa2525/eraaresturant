<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\Blog;


class PressController extends Controller
{
    public function index()
    {
        $data['posts'] = Blog::where('show','yes')->orderBy('created_at','DESC')->paginate(6);
    	return view('front.press.index')->with($data);
    }


    public function show($slug)
    {
        $row = Blog::where('slug',$slug)->first();
        if($row)
        {
            $posts = Blog::where('show','yes')->orderBy('id','DESC')->take(6)->get();
            return view('front.press.show',compact(['row','posts']));
        }
    }
}
