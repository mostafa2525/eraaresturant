<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Menu;

class MenuController extends Controller
{
    public function index()
    {
        $data['menu'] = Menu::where('show','yes')->where('special','yes')->take('12')->get();
    	$data['categories'] = Category::where('show','yes')->get();
    	return view('front.menu.index')->with($data);
    }
}
