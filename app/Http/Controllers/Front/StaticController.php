<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Menu;
use App\Models\Message;

class StaticController extends Controller
{
    public function about()
    {
    	return view('front.static.about');
    }



    public function contactUs()
    {
    	return view('front.static.contact');
    }



     // send message   
    public function sendMessage(Request $request)
    {

        if($request->ajax())
        {
            $request->validate([

                'name' => 'required|string|max:100',
                'subject' => 'required|string|max:100',
                'email' => 'required|email|max:100',
                'message' => 'required|string|max:2000',
            ]);

            $data = $request->except("_token");

            Message::create($data);
            $message['success'] = trans('front.messages.contactusMessage');
            return response()->json($message);
        }
    }



}
